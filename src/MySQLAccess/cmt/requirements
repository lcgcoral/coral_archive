package MySQLAccess

#============================================================================
# Private dependencies and build rules
#============================================================================

private

# Temporary hack to avoid c++ deprecation warnings for auto_ptr (bug #98085)
macro_append cppflags "" target-c11 " -Wno-deprecated "

#----------------------------------------------------------------------------
# Library
#----------------------------------------------------------------------------

use CoralCommon v*
use mysql * LCG_Interfaces

apply_pattern lcg_module_library

# Create a symlink .dylib -> .so on mac (sr #141482 - also see bug #37371)
apply_pattern lcg_dylib_symlink

#----------------------------------------------------------------------------
# Tests and utilities
#----------------------------------------------------------------------------

# The unit tests
apply_pattern coral_unit_test tname=BulkInserts
apply_pattern coral_unit_test tname=DataEditor
apply_pattern coral_unit_test tname=MultipleSchemas
apply_pattern coral_unit_test tname=Schema
apply_pattern coral_unit_test tname=SchemaCopy
apply_pattern coral_unit_test tname=SegFault
apply_pattern coral_unit_test tname=ShowCreateTableParser
apply_pattern coral_unit_test tname=SimpleQueries
apply_pattern coral_unit_test tname=TestAlias
apply_pattern coral_unit_test tname=Test40
apply_pattern coral_unit_test tname=Connection
apply_pattern coral_unit_test tname=NIPP

# Link some tests to plugin libraries
macro_append test_MySQLAccess_Test40linkopts ' -llcg_MySQLAccess '\
  target-winxp ' lcg_MySQLAccess.lib'
macro_append test_MySQLAccess_Connectionlinkopts ' -llcg_MySQLAccess ' \
  target-winxp ' lcg_MySQLAccess.lib '
macro_append test_MySQLAccess_NIPPlinkopts ' -llcg_MySQLAccess ' \
  target-winxp ' lcg_MySQLAccess.lib '

# Fake target for utilities
action utilities "echo No utilities in this package"
macro_remove cmt_actions_constituents "utilities"

# Fake target for examples
action examples "echo No examples in this package"
macro_remove cmt_actions_constituents "examples"
