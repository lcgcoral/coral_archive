// Include files
#include "CoralBase/Exception.h"
#include "CoralServerBase/InternalErrorException.h"
#include "RelationalAccess/SessionException.h"

// Local include files
#include "ConnectionProperties.h"
#include "DomainProperties.h"

// Namespace
using namespace coral::CoralAccess;

//-----------------------------------------------------------------------------

ConnectionProperties::ConnectionProperties( const DomainProperties& domainProperties,
                                            const std::string& coralServerUrl )
  : m_domainProperties( domainProperties )
  , m_coralServerUrl( coralServerUrl )
  , m_facade( 0 )
  , m_alive( true )
{
}

//-----------------------------------------------------------------------------

ConnectionProperties::~ConnectionProperties()
{
  m_alive = false;
}

//-----------------------------------------------------------------------------

void
ConnectionProperties::setFacade( coral::ICoralFacade* facade )
{
  m_facade = facade;
}

//-----------------------------------------------------------------------------

const DomainProperties& ConnectionProperties::domainProperties() const
{
  return m_domainProperties;
}

//-----------------------------------------------------------------------------

std::string ConnectionProperties::coralServerUrl() const
{
  return m_coralServerUrl;
}

//-----------------------------------------------------------------------------

bool ConnectionProperties::isConnected() const
{
  return ( m_facade != 0 );
}

//-----------------------------------------------------------------------------

const coral::ICoralFacade&
ConnectionProperties::facade() const
{
  if ( !m_alive ) // debug CORALCOOL-1050: we are "lucky" this did not crash...
    throw coral::InternalErrorException( "PANIC! This instance has already been deleted! (CORALCOOL-1050)",
                                         "ConnectionProperties::facade" ,
                                         domainProperties().service()->name() );
  if ( !m_facade )
    throw coral::ConnectionException( domainProperties().service()->name(),
                                      "ConnectionProperties::facade" ,
                                      "No facade is available" );
  return *m_facade;
}

//-----------------------------------------------------------------------------
