//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id: ServerReaderFactory.cpp,v 1.2.2.4 2012-11-20 22:27:51 salnikov Exp $
//
// Description:
//	Class ServerReaderFactory...
//
// Author List:
//      Andrei Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "ServerReaderFactory.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cerrno>
#include <cstring>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <sys/socket.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MsgLogger.h"
#include "NetEndpointAddress.h"
#include "NetSocket.h"
#include "ServerReader.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace coral {
namespace CoralServerProxy {

//----------------
// Constructors --
//----------------
ServerReaderFactory::ServerReaderFactory (const std::vector<NetEndpointAddress>& serverAddress,
                                          PacketQueue& rcvQueue,
                                          unsigned timeoutSec)
  : m_serverAddress( serverAddress )
  , m_rcvQueue( rcvQueue )
  , m_timeoutSec ( timeoutSec )
{
}

//--------------
// Destructor --
//--------------
ServerReaderFactory::~ServerReaderFactory ()
{
}

// opens connection to upstream server, returns -1 on failure
NetSocket
ServerReaderFactory::serverConnect() const
{
 // create socket
  PXY_DEBUG ( "ServerReaderFactory: Creating server-side socket" );
  NetSocket sock ( PF_INET, SOCK_STREAM, 0 ) ;
  if ( not sock.isOpen() ) {
    PXY_ERR ( "ServerReaderFactory: Failed to create a socket: " << strerror(errno) );
    return sock ;
  }

  // set socket options
  if ( setSocketOptions( sock ) < 0 ) {
    PXY_ERR ( "ServerReaderFactory: Failed to set socket options " << sock << ": " << strerror(errno) );
    sock.close() ;
    return sock ;
  }

  bool open = false;
  for (unsigned i = 0; i != m_serverAddress.size(); ++ i) {
    // always take first entry, the queue is updated below
    NetEndpointAddress endpoint = m_serverAddress.front();
    if (sock.connect(endpoint) < 0) {
      PXY_WARN( "ServerReaderFactory: Failed to connect to server " << endpoint <<  ": " << strerror(errno)
                << " (will retry other servers if any)");
      // move failed endpoint to the end of the queue
      m_serverAddress.erase(m_serverAddress.begin());
      m_serverAddress.push_back(endpoint);
    } else {
      PXY_INFO ( "ServerReaderFactory: Connected to server " << endpoint <<  " " << sock );
      open = true;
      break;
    }
  }
  if (! open) {
    PXY_ERR ( "ServerReaderFactory: Failed to connect to any upstream server");
    sock.close() ;
  }

  return sock ;
}  

// start new thread serving the server socket
std::shared_ptr<coral::thread> 
ServerReaderFactory::readerThread(ClientConnManager& connManager) const
{
  return std::make_shared<coral::thread>(ServerReader(connManager, m_rcvQueue, m_timeoutSec));
}

int
ServerReaderFactory::setSocketOptions( NetSocket& sock ) const
{
  // reuse the address
  if ( sock.setSocketOptions ( SOL_SOCKET, SO_REUSEADDR, 1 ) < 0 ) {
    return -1 ;
  }
  if ( sock.setSocketOptions ( SOL_SOCKET, SO_KEEPALIVE, 1 ) < 0 ) {
    return -1 ;
  }
  // disable Naggle algorithm
  if ( sock.setSocketOptions ( IPPROTO_TCP, TCP_NODELAY, 1 ) < 0 ) {
    return -1 ;
  }
  return 0 ;
}

} // namespace CoralServerProxy
} // namespace coral
