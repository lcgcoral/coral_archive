#ifndef CORALSERVERPROXY_NETPORT_H
#define CORALSERVERPROXY_NETPORT_H

//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id: NetPort.h,v 1.1.2.2 2010-05-26 08:12:41 avalassi Exp $
//
// Description:
//	Class NetPort.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

/**
 *  Port can be specified either as a fixed TCP port number (one integer)
 *  or as a portmapper entry with program number and version number pair.
 *
 *  @version $Id: NetPort.h,v 1.1.2.2 2010-05-26 08:12:41 avalassi Exp $
 *
 *  @author Andy Salnikov
 */

namespace coral {
namespace CoralServerProxy {

class NetPort  {
public:

  // parse NUL-terminated string and return port instance, returns non-valid
  // port instance if parsing fails
  static NetPort parse(const char* portStr);

  // Default constructor makes unusable port
  NetPort() : m_port(0), m_pmapPrognum(0), m_pmapVersion(0) {}

  // Make port from explicit port number
  explicit NetPort(unsigned short port) :
          m_port(port), m_pmapPrognum(0), m_pmapVersion(0) {}

  // Make port from portmapper program number
  NetPort(unsigned long pmapPrognum, unsigned long pmapVersion) :
          m_port(0), m_pmapPrognum(pmapPrognum), m_pmapVersion(pmapVersion) {}

  // returns true if this object represents valid port number
  bool isValid() const {
      return m_port != 0 || m_pmapPrognum != 0;
  }

  unsigned short port() const { return m_port; }
  unsigned long pmapPrognum() const { return m_pmapPrognum; }
  unsigned long pmapVersion() const { return m_pmapVersion; }
  bool hasPortmap() const { return m_pmapPrognum != 0; }

  void print(std::ostream& out) const;

private:

  // Data members

  unsigned short m_port;  // regular port number
  unsigned long m_pmapPrognum;
  unsigned long m_pmapVersion;

};

// printing of the address in a nice format
inline std::ostream&
operator<<(std::ostream& out, const NetPort& a) {
    a.print(out);
    return out;
}

} // namespace CoralServerProxy
} // namespace coral

#endif // CORALSERVERPROXY_NETPORT_H
