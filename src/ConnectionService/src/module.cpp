#include "CoralKernel/CoralPluginDef.h"
#include "ConnectionService.h"

CORAL_PLUGIN_MODULE( "CORAL/Services/ConnectionService", coral::ConnectionService::ConnectionService )

CORAL_PLUGIN2_MODULE( "CORAL/Services/ConnectionService2", coral::ConnectionService::ConnectionService )
