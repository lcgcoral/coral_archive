//-----------------------------------------------------------------------------
// This test was initially developed for the FrontierAccess plugin, but was
// ported to all plugins. It is aimed at checking if monitoring of the plugins
// is able to produce a standard output report containing the operation
// summary of the session.
//-----------------------------------------------------------------------------

// Include files
#include <cmath>
#include <cstdlib>
#include <memory>
#include <string>
#include "CoralBase/Exception.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeSpecification.h"
#include "CoralBase/Blob.h"
#include "CoralBase/TimeStamp.h"
#include "CoralBase/../tests/Common/CoralCppUnitDBTest.h"
#include "RelationalAccess/AccessMode.h"
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ConnectionServiceException.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IDatabaseServiceDescription.h"
#include "RelationalAccess/IDatabaseServiceSet.h"
#include "RelationalAccess/ILookupService.h"
#include "RelationalAccess/IMonitoringReporter.h"
#include "RelationalAccess/IMonitoringService.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/IRelationalDomain.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/ITableSchemaEditor.h"
#include "RelationalAccess/ITablePrivilegeManager.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/MonitoringException.h"
#include "RelationalAccess/RelationalServiceException.h"
#include "RelationalAccess/TableDescription.h"

namespace coral
{

  class MonitorControllerTest : public CoralCppUnitDBTest
  {

    CPPUNIT_TEST_SUITE( MonitorControllerTest );
    // NB Use the alternative COOL URL only in the 1st test
    // Keep the monitoring data for the main CORAL URL empty for the 2nd test
    CPPUNIT_TEST( test_MonitoringDisabled ); // THIS MUST BE THE FIRST TEST
    CPPUNIT_TEST( test_MonitorController );
    CPPUNIT_TEST_SUITE_END();

  public:

    const std::string T1 = BuildUniqueTableName( "T" );

    const std::string getPhysicalUrl( const std::string& url, bool readOnly )
    {
      IConnectionServiceConfiguration& config = connSvc().configuration();
      //----- Get the physical connection string
      const ILookupService& lookupSvc = config.lookupService();
      std::string physUrl = url;
      if ( url.find("sqlite_file") != 0 )
      {
        IDatabaseServiceSet* dbSet = lookupSvc.lookup( url, ( readOnly ? ReadOnly : Update ) );
        if( dbSet->numberOfReplicas() == 0 )
        {
          throw std::runtime_error( "No replicas found for " + url );
        }
        physUrl = dbSet->replica( 0 ).connectionString();
      }
      return physUrl;
    }    

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    void writeData()
    {
      std::auto_ptr<ISessionProxy> session( connSvc().connect( UrlRW(), coral::Update ) );
      // Create table description
      coral::TableDescription description1( "SchemaDefinition_Test" );
      description1.setName( T1 );
      description1.insertColumn( "id", coral::AttributeSpecification::typeNameForId( typeid(long) ) );
      description1.setPrimaryKey( "id" ); // primary key
      description1.insertColumn( "t", coral::AttributeSpecification::typeNameForId( typeid(short) ) );
      description1.insertColumn( "Xx", coral::AttributeSpecification::typeNameForId( typeid(float) ) );
      description1.setNotNullConstraint( "Xx" ); // not null constraint
      description1.insertColumn( "Y", coral::AttributeSpecification::typeNameForId( typeid(double) ) ); 
      description1.setUniqueConstraint( "Y", "U_" + T1 + "_Y" ); // unique key
      description1.insertColumn( "Z", coral::AttributeSpecification::typeNameForId( typeid(double) ) );
      description1.insertColumn( "Comment", coral::AttributeSpecification::typeNameForId( typeid(std::string) ), 100, false ); // variable size string
      description1.insertColumn( "Data", coral::AttributeSpecification::typeNameForId( typeid(coral::Blob) ) ); // BLOB
      // Create table
      session->transaction().start();
      session->nominalSchema().dropIfExistsTable( T1 );
      coral::ITable& table = session->nominalSchema().createTable( description1 );
      // Modify the table (DDL)
      table.schemaEditor().renameColumn( "Xx", "X" ); // change column name
      table.schemaEditor().changeColumnType( "t", coral::AttributeSpecification::typeNameForId( typeid(long long) ) ); // change column type
      coral::ITable* ptable = &table; // switch to a pointer because "table=" for MySQL would change the object, not the reference to it!!! http://stackoverflow.com/questions/7713266
      if ( UrlRW() == BuildUrl( "MySQL", false ) )
      {
        // For MySQL must commit column renaming from Xx to X and column type 
        // change for t from short to long long and reload the table properties
        // in a new Table instance before using the new column name and type 
        // to set two new constraints (CORALCOOL-1088)
        session->transaction().commit();
        session.reset( connSvc().connect( UrlRW(), coral::Update ) );
        session->transaction().start();
        //table = session->nominalSchema().tableHandle( T1 ); // WRONG! RESULTS IN SEGFAULTS!
        ptable = &(session->nominalSchema().tableHandle( T1 ));
      }
      ptable->schemaEditor().setNotNullConstraint( "t" ); // add not null constraint
      std::vector< std::string > constraintColumns( 2 );
      constraintColumns[0] = "X";
      constraintColumns[1] = "Z";
      ptable->schemaEditor().setUniqueConstraint( constraintColumns ); // two-column unique key
      //table.schemaEditor().setUniqueConstraint( constraintColumns ); // SEGFAULT!
      ptable->schemaEditor().createIndex( T1 + "_IDX_t", "t", true ); // unique index
      ptable->privilegeManager().grantToPublic( coral::ITablePrivilegeManager::Select ); // grant privileges
      // Modify table contents (DML)
      coral::ITableDataEditor& editor1 = ptable->dataEditor();
      coral::AttributeList rowBuffer1;
      rowBuffer1.extend<long>( "id" );
      long& id = rowBuffer1[ "id" ].data<long>();
      rowBuffer1.extend< long long >( "t" );
      long long& t = rowBuffer1[ "t" ].data<long long>();
      rowBuffer1.extend< float >( "X" );
      float& x = rowBuffer1[ "X" ].data<float>();
      rowBuffer1.extend< double >( "Y" );
      double& y = rowBuffer1[ "Y" ].data<double>();
      rowBuffer1.extend< double >( "Z" );
      double& z = rowBuffer1[ "Z" ].data<double>();
      rowBuffer1.extend< std::string >( "Comment" );
      std::string& comment = rowBuffer1[ "Comment" ].data<std::string>();
      rowBuffer1.extend< coral::Blob >( "Data" );
      coral::Blob& blob = rowBuffer1[ "Data" ].data<coral::Blob>();
      for ( int i = 0; i < 10; ++i ) 
      {
        id = i + 1;
        t = 1;
        t <<= 4*i;
        x = 1 + (i+1)*(float)0.001;
        y = 2 + (i+1)*0.0001;
        z = i*1.1;
        std::ostringstream os;
        os << "Row " << i + 1;
        comment = os.str();
        //int blobSize = 1000 * ( i + 1 );
        int blobSize = 10 * ( i + 1 ); // limit the blob size to debug other MySQL issues... (CORALCOOL-1088)
        blob.resize( blobSize );
        unsigned char* p = static_cast<unsigned char*>( blob.startingAddress() );
        for ( int j = 0; j < blobSize; ++j, ++p ) *p = ( i + j )%256;
        if ( i%4 == 2 ) {
          rowBuffer1[ "Z" ].setNull( true );
        }
        else {
          rowBuffer1[ "Z" ].setNull( false );
        }
        //std::cout << "Insert new row " << i << " id=" << id << " t=" << t << " Y=" << y << " X,Z=" << x << "," << z << std::endl;
        editor1.insertRow( rowBuffer1 );
      }
      session->transaction().commit();
      coral::sleepSeconds( 1 );
    }

    void readData()
    {
      std::auto_ptr<ISessionProxy> session( connSvc().connect( UrlRO(), coral::ReadOnly ) );
      session->transaction().start(true);
      for ( int i = 0; i < 10; ++i ) 
      {
        coral::IQuery* query0 = session->nominalSchema().tableHandle( T1 ).newQuery();
        query0->addToOutputList( "Data" );
        query0->addToOutputList( "Z" );
        query0->addToOrderList( "id" );
        coral::ICursor& cursor0 = query0->execute();
        while ( cursor0.next() ) {}
        //if ( i == 5 ) session->monitoringController().stop();
        delete query0;
      }
      session->transaction().commit();
    }

    void
    test_MonitorController()
    {
      const std::string physUrlRO = getPhysicalUrl( UrlRO(), true );

      //----- Configure the Monitoring Service
      // [NB: ConnectionService::ConnectionHandle::newSession starts session]
      // [monitoring by calling MonitorController::start if monitoring enabled]
      // Levels are Off, Minimal, Default, Debug, Trace
#define MONLVL Default
      std::cout << "+++ Start monitoring (level=MONLVL)" << std::endl;
      IConnectionServiceConfiguration& config = connSvc().configuration();
      //config.setMonitoringService( "CORAL/Services/MonitoringService" ); // this now throws all the time (CORALCOOL-2974), and this call was useless anyway!
      //config.monitoringService(); // useless: comment it out to test that it is NOT needed!
      config.setMonitoringLevel(monitor::MONLVL); // start monitoring
      monitor::Level monLev = config.monitoringLevel();
      std::cout << "+++ Monitoring level is " << monLev << std::endl;

      // ----- Write data -----
      std::cout << "=== Write data" << std::endl;
      writeData();

      // ----- Read data -----
      std::cout << "=== Read data" << std::endl;
      readData();
      std::cout << "+++ Stop monitoring (level=Off)" << std::endl;
      config.setMonitoringLevel(monitor::Off); // "stop" monitoring
      monLev = config.monitoringLevel();
      std::cout << "+++ Monitoring level is " << monLev << std::endl;
      
      // ----- Report all sessions to the MessageService (CORALCOOL-2971) -----
      std::cout << "=== Dump report for all sessions via the MessageService" << std::endl;
      coral::MsgLevel oldLvl = coral::MessageStream::msgVerbosity();
      coral::MessageStream::setMsgVerbosity( coral::Verbose );
      IMonitoringReporter const& reporter = connSvc().monitoringReporter();
      reporter.report( monitor::Trace );
      coral::MessageStream::setMsgVerbosity( oldLvl );

      // ----- Report this session to std::cout -----
      std::cout << "=== Dump report to stdout (level=Trace) for " << physUrlRO << std::endl;
      reporter.reportToOutputStream( physUrlRO, std::cout , monitor::Trace );
      std::cout << std::endl;
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    // NB *** THIS IS EXECUTED AS THE FIRST TEST IN THIS SUITE! ***
    // [Behaviour changes depending on the order: has the MonSvc already
    // been loaded or not, is there monitoring data for a given URL or not]
    void
    test_MonitoringDisabled() // Test for CORALCOOL-2942 within CORALCOOL-1088
    {
      IConnectionServiceConfiguration& config = connSvc().configuration();
      // Setting the monitoring service name now always throws (CORALCOOL-2974)
      // This call here was completely useless anyway!
      CPPUNIT_ASSERT_THROW_MESSAGE( "changing the MonSvc name should ALWAYS throw", config.setMonitoringService( "CORAL/Services/MonitoringService" ), coral::Exception );
      // Determine the URLs for this test
      coral::AccessMode mode;
      std::string url2; // standard CORAL url for the 2nd test
      std::string url1; // non-standard url (e.g. COOL url) for the 1st test
      if ( UrlRO().find("sqlite_file") == 0 )
      {
        mode = coral::Update; // else connections fail if files do not exist!
        url2 = UrlRO();
        url1 = UrlRO()+"2";
      }
      else
      {
        mode = coral::ReadOnly;
        url2 = getPhysicalUrl( UrlRO(), true );
        // Assume from CoralCppUnitDBTest.h that UrlRO is CORAL-xxx/reader
        // Transform this into COOL-xxx to use the COOL account for this test!
        CPPUNIT_ASSERT_EQUAL_MESSAGE( "UrlRO starts with CORAL", UrlRO().substr(0,5), std::string("CORAL") );
        CPPUNIT_ASSERT_EQUAL_MESSAGE( "UrlRO ends with /reader", UrlRO().substr(UrlRO().size()-7), std::string("/reader") );
        url1 = getPhysicalUrl( "COOL" + UrlRO().substr(5,UrlRO().size()-12), true );
      }
      // Level is off to start with
      std::cout << "__ Monitoring level is Off" << std::endl;
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "monlvl==0", monitor::Level::Off, config.monitoringLevel() );
      // Reporting for a session should fail if the session has not started!
      std::cout << "__ Do not start a session yet" << std::endl;
      {
        IMonitoringReporter const& reporter = connSvc().monitoringReporter();
        CPPUNIT_ASSERT_THROW_MESSAGE( "report should throw", reporter.reportToOutputStream( url1, std::cout , monitor::Trace ), coral::MonitoringException );
      }
      // Reporting for a session should fail if monitoring level is off
      std::cout << "__ Now connect and start the first session" << std::endl;
      {
        // NB: MonitoringService is now loaded in the first call to connect.
        // It was previously loaded in the first call to monitoringReporter,
        // leading to unexpected results in this test (CORALCOOL-2969).
        ISessionProxy* session = connSvc().connect( url1, mode );
        delete session;
        IMonitoringReporter const& reporter = connSvc().monitoringReporter();
        // Test failed here (due to CORALCOOL-2942) after fixing CORALCOOL-2969
        CPPUNIT_ASSERT_THROW_MESSAGE( "report should throw", reporter.reportToOutputStream( url1, std::cout , monitor::Trace ), coral::MonitoringException );
      }
      // Level is still off
      std::cout << "__ Monitoring level is still Off" << std::endl;
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "monlvl==0 still", monitor::Level::Off, config.monitoringLevel() );
      // Reporting for a session should STILL fail if level is still off!
      std::cout << "__ Connect and start another session" << std::endl;
      {
        ISessionProxy* session = connSvc().connect( url1, mode );
        delete session;
        IMonitoringReporter const& reporter = connSvc().monitoringReporter();
        // Test was failing here before fixing CORALCOOL-2969
        CPPUNIT_ASSERT_THROW_MESSAGE( "report should still throw", reporter.reportToOutputStream( url1, std::cout , monitor::Trace ), coral::MonitoringException );
      }
      // Level is now on
      std::cout << "__ Set monitoring level to Trace" << std::endl;
      config.setMonitoringLevel( monitor::Level::Trace );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "monlvl==Trace", monitor::Level::Trace, config.monitoringLevel() );
      // Reporting for a session should succeed if level is not Off
      std::cout << "__ Connect and start another session" << std::endl;
      {
        ISessionProxy* session = connSvc().connect( url1, mode );
        delete session;
        IMonitoringReporter const& reporter = connSvc().monitoringReporter();
        reporter.reportToOutputStream( url1, std::cout , monitor::Trace );
      }
      // Check that you cannot change the monitoring service name now
      CPPUNIT_ASSERT_THROW_MESSAGE( "changing the MonSvc name here should throw", config.setMonitoringService( "CORAL/Services/MonitoringService" ), coral::Exception );
      // Level is off again - check that this affects new sessions
      std::cout << "__ Monitoring level is Off again" << std::endl;
      config.setMonitoringLevel( monitor::Level::Off );
      CPPUNIT_ASSERT_EQUAL_MESSAGE( "monlvl==0", monitor::Level::Off, config.monitoringLevel() );
      // Reporting for a session should fail if monitoring level is off
      std::cout << "__ Now connect to a different URL2" << std::endl;
      {
        ISessionProxy* session = connSvc().connect( url2, mode );
        delete session;
        IMonitoringReporter const& reporter = connSvc().monitoringReporter();
        CPPUNIT_ASSERT_THROW_MESSAGE( "report for url2 should throw", reporter.reportToOutputStream( url2, std::cout , monitor::Trace ), coral::MonitoringException );
        // Reporting for the previous session however should work!
        // Global monitoring level off does not change already collected data!
        std::cout << "__ Report again for URL1" << std::endl;
        reporter.reportToOutputStream( url1, std::cout , monitor::Trace );
      }
    }
    
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    void setUp()
    {
      //----- Configure the Connection Service for all tests
      IConnectionServiceConfiguration& config = connSvc().configuration();
      config.disableReplicaFailOver();
      config.setConnectionTimeOut(0); // close expired connections immediately
      setenv( "CORAL_CONNECTIONPOOL_VERBOSE", "1", 1 );
    }

    void tearDown()
    {
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  };

  CPPUNIT_TEST_SUITE_REGISTRATION( MonitorControllerTest );

}

CORALCPPUNITTEST_MAIN( MonitorControllerTest )
