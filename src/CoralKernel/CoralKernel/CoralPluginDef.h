#ifndef CORALKERNEL_CORALPLUGINDEF_H
#define CORALKERNEL_CORALPLUGINDEF_H 1

// Include files
#include "CoralKernel/ILoadableComponent.h"
#include "CoralKernel/ILoadableComponentFactory.h"

namespace coral {
  template<typename T> class CoralPluginFactory : virtual public coral::ILoadableComponentFactory
  {
  public:
    CoralPluginFactory( const std::string& name ) : m_name( name ) {}
    virtual ~CoralPluginFactory() {}
    coral::ILoadableComponent* component() const
    { return static_cast< coral::ILoadableComponent* >( new T( m_name ) ); }
    std::string name() const { return m_name; }
  private:
    std::string m_name;
  };
}


// NEW IMPLEMENTATION: inside shared library, locate ptr to method
// that instantiates a static factory (fix clang bug #92167)
// [see also http://tldp.org/HOWTO/C++-dlopen/thesolution.html]
#define CORAL_PLUGIN_MODULE(NAME,PLUGINCLASS)                           \
  extern "C" { coral::ILoadableComponentFactory* coral_component_factory() { \
    static coral::CoralPluginFactory< PLUGINCLASS > theFactory( std::string( NAME ) ); return &theFactory; } }
#define CORAL_PLUGIN2_MODULE(NAME,PLUGINCLASS)                          \
  extern "C" { coral::ILoadableComponentFactory* coral_component_factory2() { \
    static coral::CoralPluginFactory< PLUGINCLASS > theFactory2( std::string( NAME ) ); return &theFactory2; } }

#endif
